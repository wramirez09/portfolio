
import React, { Fragment } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

import About from './js/pages/About'
import Contact from './js/pages/Contact'
import Work from './js/pages/Work'
import Home from './js/pages/Home';
import 'bootstrap/dist/css/bootstrap.css'; 
import './App.scss';
import gsap from 'gsap';
import { ScrollToPlugin } from 'gsap/all';
import {ContextProvider} from './js/components/Context';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

gsap.registerPlugin(ScrollToPlugin)

function App() {
  
  const menu = [
    {
      link:"/",
      text:"Home"
    },
    {
      link:"/about/",
      text:"About"
    },
    {
      link:"/work/",
      text:"Work"
    },
    {
      link:"/contact/",
      text:"Contact"
    }
  ]

  let loaderVal = {loaderVisible:true};

  return (
  <ContextProvider value={loaderVal}>
    <Fragment>
       <Router>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" sticky="top">
            <Navbar.Brand href="#home">Will Ramirez</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
          
                  {menu.map((link, iteration)=>{

                  return (
                      <Link className="navbar__link" key={iteration + 1} to={link.link}>{link.text}</Link>
                  )
                  })}
          
              </Nav>
              </Navbar.Collapse> 
            </Navbar>
            <Switch>
                <Route exact={true} path="/">
                      <Home />
                </Route>
                <Route path="/about" >
                      <About />
                </Route>
                <Route path="/Work">
                      <Work />
                </Route>
                <Route path="/Contact" >
                      <Contact />
                </Route>
            </Switch>
        </Router>
     

</Fragment>
</ContextProvider> 
  );
}

export default App;
