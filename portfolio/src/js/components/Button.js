import React,{ Fragment } from 'react';



const Button = (props)=>{

    return (

        <a className="button" href={props.url}>{props.text}</a>
    )
}


export default Button;