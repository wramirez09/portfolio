
import React, { Fragment } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

import About from '../pages/About'
import Contact from '../pages/Contact'
import Work from '../pages/Work'
import Home from '../pages/Home'


const Navigation = (props)=>{

    const HTML_SELECTOR_MAIN = 'navigation';
    const HTML_SELECTOR_BUTTON = 'navigation__hamburger-button';
    const TOGGLE_CLASS = 'open'

    const toggleActiveClass = (e) =>{

        document.querySelector(`.${HTML_SELECTOR_BUTTON}`).classList.toggle(TOGGLE_CLASS);
        activateMenu();
    };

    const activateMenu = ()=>{

        const bodyTag = document.querySelector('body');

        if(bodyTag.classList.contains(TOGGLE_CLASS)){

            bodyTag.classList.remove(TOGGLE_CLASS)
        }
        else {
            bodyTag.classList.add(TOGGLE_CLASS)
        }
    }

    return (

        <Fragment>
            
            <section className={HTML_SELECTOR_MAIN}>
                
                <nav className={HTML_SELECTOR_BUTTON} onClick={toggleActiveClass}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </nav>
            </section>
            <section className='navigation__menu'>
                <Router>
                    <nav>
                        <ul className="navigation__menu-list">
                            {props.menu.map((link, iteration)=>{

                            return (
                                <li key={iteration + 1}><Link to={link.link}>{link.text}</Link></li>
                            )
                            })}
                        </ul>
                    </nav>
                    <Switch>
                        <Route exact="/" path="/" component={Home} />
                        <Route path="/about-us/" component={About} />
                        <Route path="/Work/" component={Work} />
                        <Route path="/Contact/" component={Contact}/>
                    </Switch>
                </Router>
            </section>
        </Fragment>
    )
}

export default Navigation;