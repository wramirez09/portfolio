import React, { Fragment } from 'react';
import gsap from 'gsap';

 const Photographer = (props)=>{

    const photographerDiv = document.querySelector('.unsplash__photographer');

    const displaydata = ()=>{

        if(photographerDiv){
            
            gsap.to([photographerDiv], 2, {delay: 1, opacity:1, ease: "bounce.ou(1.7)"});
        }
        
        return (

            <section className='unsplash__photographer'>
                <img className="unsplash__port-pic" src={props.profile_image} alt=""/> 
                <p><strong>Photographer:</strong> {props.first_name} {props.last_name} </p>
                <p> <strong>Bio:&nbsp;</strong>{props.bio} </p>
                <p><strong>Portfolio:&nbsp;</strong><a href={props.portfolio_url}>{props.portfolio_url}</a> </p>
            </section>
        )
    }

    return (
        <Fragment>
           {
            displaydata()
           }
        </Fragment>
    )
}
export default Photographer;