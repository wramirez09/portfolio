import React, {useState, useEffect, Fragment } from 'react';
// import aboutbg from '../../images/about_ph.jpeg';
import Photographer from './Photographer';
// import ScrollMagic from "scrollmagic"; 
// import { ScrollMagicPluginGsap } from "scrollmagic-plugin-gsap";
import CalAspectRation from './CalAspectRatio';
import Axios from "axios";
import FrostImage from "./FrostImage";
import Loader from './Loader';


let car = CalAspectRation();

const AboutSection = ()=>{
    const [bgImageData, setBgImageData] = useState({});
    const endpoint = 'http://localhost:8000/unsplash';
    const client = window || null;
    const clientHeight = client.innerHeight;
    const imageData = {
            place: "web design",
            orientation: car,
        }


        let init = ()=>{
            

            console.log("about sec init")
             Axios.post(endpoint, {
                 data:imageData
             }).then((imagesArray)=>{
                 
                function getRandomArbitrary(min, max) {
                    return Math.random() * (max - min) + min;
                }
    
                const rn = Math.floor(getRandomArbitrary(0, 9));
                
                if(imagesArray.data.length !== 0){
    
                    // window.localStorage.setItem("imageDataArray", imagesArray.toString());
    
                    const bgImagedata  = {
                        imageUrl: CalAspectRation() === "landscape" ? imagesArray.data[rn].urls.full : imagesArray.data[rn].urls.small,
                        location: imagesArray.data[rn].location,
                        first_name: imagesArray.data[rn].user.first_name,
                        last_name: imagesArray.data[rn].user.last_name,
                        profile_image: imagesArray.data[rn].user.profile_image.small,
                        bio:imagesArray.data[rn].user.bio,
                        portfolio_url:imagesArray.data[rn].user.portfolio_url,
                    }
    
                    setBgImageData(bgImagedata);
                    
                }
             }).catch((e)=>{
                return console.log('error', e)
             })
        }

        

        useEffect(()=>{
            init();
        }, [])

        return (

               <Fragment>
                   {
                       (()=>{
                           if(bgImageData){
                               return <section className="aboutSection" style={{background:`url(${bgImageData.imageUrl})`, height:clientHeight}}>
                               <FrostImage buttonText="Work" headline={"about me"} backgroundImage={bgImageData.imageUrl}></FrostImage>
                               <Photographer></Photographer>

                                </section>
                           }
                           else{

                               return <Loader></Loader>
                           }
                       })()
                   }
                   
               </Fragment>
                

                
            
        )
};

export default AboutSection;