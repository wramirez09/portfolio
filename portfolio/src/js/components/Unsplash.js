import React, { useState, useEffect, Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Axios from 'axios';
import Photographer from '../components/Photographer';
import RenderData from '../components/RenderData';
import FrostImage from './FrostImage';
import caret from '../../images/down-arrow.svg';
import gsap from 'gsap';
import { ScrollToPlugin } from 'gsap/all';
import Loader from '../components/Loader';
import CalAspectRation from "../components/CalAspectRatio";
import Jumbotron from 'react-bootstrap/Jumbotron';
import {toggleLoader} from "../Redux/Reducers/loaderSlice"
gsap.registerPlugin(ScrollToPlugin);

//redux

const Unsplash = (props)=>{
    const loaderState = useSelector(state => state.toggleLoaderReducer.toggleLoader);
    const dispatch = useDispatch();
    // dispatch();
    const endpoint = 'http://localhost:8000/unsplash';
    const [bgImage, setBgImage] = useState({}); 
    const [data, setdata] = useState(null);
    const [hookedLoaderState, setLoaderState] = useState(loaderState);
    
    let init = ()=> {
        
        if(!navigator.geolocation) {
           
           console.log('no location support');
           
        }
        else {

            if(window.localStorage.getItem("dataImageArray" !== "")){
                success(JSON.parse(window.localStorage.getItem("dataImageArray")))
            }
            else{
                navigator.geolocation.getCurrentPosition(success, error);
            }
            
        }
    }
    
    let success = async(pos)=>{
        
        const endpoint = `http://localhost:8000/getlocation`;
        
        

         await Axios({
            method: 'post',
            url: endpoint,
            data: {
              latitude: pos.coords.latitude,
              longitude: pos.coords.longitude,
            }
          }).then((data)=>{     
        
            setdata(data);
            getBg(data);
            dispatch(toggleLoader());
            
          })
    }

    let error = (e)=> {
        console.log('error ', e)
    }

    let getBg = async (location)=>{

        const imageData = {
            place: location.data[0].adminArea5,
            orientation:CalAspectRation()
        }
        
        await Axios.post(endpoint, {

            data: imageData,

        }).then((images)=>{

            function getRandomArbitrary(min, max) {
                return Math.random() * (max - min) + min;
            }

            const rn = Math.floor(getRandomArbitrary(0, 9));
            
            if(images.data.length !== 0){

                window.localStorage.setItem("imageDataArray", images.toString());

                const bgImagedata  = {
                    imageUrl: CalAspectRation() === "landscape" ? images.data[rn].urls.full : images.data[rn].urls.small,
                    location: images.data[rn].location,
                    first_name: images.data[rn].user.first_name,
                    last_name: images.data[rn].user.last_name,
                    profile_image: images.data[rn].user.profile_image.small,
                    bio:images.data[rn].user.bio,
                    portfolio_url:images.data[rn].user.portfolio_url,
                }

                setBgImage(bgImagedata)
            }

        })
        
        
    }


    // render views 

    let showCaret = ()=>{

        const caret = document.querySelector('.unsplash__caret');

        if(caret){
            gsap.to([caret], 1, {opacity: 1, ease: 'bounce.out'})

        }
        
    }
    let scrollToAboutSection = ()=>{
        console.log('caret click')
        if(window){
            gsap.to([window], 1, {scrollTo: '.aboutSection', onComplete:()=>{
                gsap.to(['.aboutSection__copy'], 1, {opacity:1})
            }});
            
        }
        
    }

    let unsplash = (data)=>{

        const unsplash = document.querySelector('.unsplash');
        const client = window || null;
        const clientHeight = client.innerHeight;
        

        if(unsplash){
            
            gsap.to([unsplash], .5, {opacity:1});
            showCaret();
        }

    const heroHeight = clientHeight + "px";

        if(bgImage){
            return(

                <Fragment>
                    <Jumbotron fluid={true} style={{background:`url(${bgImage.imageUrl}) no-repeat`, width:"100%", height:`${heroHeight}`}} >
                            <FrostImage headline={props.headline.__html} backgroundImage={bgImage.imageUrl} buttonText="About"></FrostImage>
                            <img src={caret}  width="50px" height="40px" alt="down-arrow" className='unsplash__caret' onClick={scrollToAboutSection}/>
                            <RenderData data={data}></RenderData> 
                    </Jumbotron>
                   
                    <Photographer {...bgImage}></Photographer>
                </Fragment>
    
            )
        }
        
    }


    let showLoader = ()=>{

        return <Loader></Loader>
    }

    useEffect(()=>{
        init();
        // setContext.setContext({isloaderVisible:false})
        
    }, []);
  

    return(
       <Fragment>
           {
               ((data, props, dispatch)=>{
                        
                        
                    if(data){
                        {/* dispatch(toggleLoader()); */}
                        {/* console.log('updated loader state', loaderState) */}
                        return unsplash(data);
                    }
                    else{
                        console.log('no data yet')
                        return showLoader();
                    }

               })(data, props, dispatch)
           }
       </Fragment>
    )

}

export default Unsplash;