import React from 'react';
import {
    Link
  } from "react-router-dom";
import Button from '@material-ui/core/Button';
const FrostImage = (props)=>{

    
    let escape_headline = ()=>{

        return {__html: props.headline};
    }
    

    return (
        <section>
            <section className="headline"><span dangerouslySetInnerHTML={escape_headline()} /></section>
            <section style={{backgroundImage:`url(${props.backgroundImage})`}} className='frost'>  */}
            </section>
            
            <Button className="unsplash__button" variant="contained" color="default">
            About
            </Button>      
            <Link className="MuiButtonBase-root MuiButton-root MuiButton-contained unsplash__button"  to="/about/">{props.buttonText}</Link>
        
        </section>  
    
    )

}

export default FrostImage;