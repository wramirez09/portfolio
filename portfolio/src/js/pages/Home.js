import React,{ Fragment } from 'react';
import Unsplash from '../components/Unsplash';
import AboutSection from '../components/AboutSection';
import { useSelector, useDispatch } from 'react-redux';
// import Clients from '../components/ClientSection';



const Home = ()=>{
    const loaderState = useSelector(state => state.toggleLoaderReducer.toggleLoader);
    const headline = `“I want to make beautiful things,<br> even if nobody cares.”<br> <small>― Saul Bass.</small>`;
    let createMarkup = ()=> { return {__html: headline} };
    
    return (
        <Fragment>
            <Unsplash headline={createMarkup()}></Unsplash>
            {
                (()=>{
                    if(loaderState === false){
                        return <AboutSection></AboutSection>
                    }
                })()
            }
            
            {/* <Clients></Clients> */}
        </Fragment>
    )
};

export default Home;