import { configureStore } from '@reduxjs/toolkit'

import toggleLoaderReducer from './Reducers/loaderSlice'

export default configureStore({
  reducer: {
    toggleLoaderReducer: toggleLoaderReducer
  }
})