
require('dotenv').config();
const GetCityData = require('../API/getCityData');
const GetLinkedInProfile = require("./GetLinkedInProfile");
const fetch = require('node-fetch');
const cors = require('cors')
global.fetch = fetch;
const Unsplash = require('../API/Unsplash');
const express = require('express');
const app = express();
const bodyParser = require('body-parser')
// create application/json parser
const jsonParser = bodyParser.json()

app.use(cors());

app.post('/unsplash', jsonParser, async function (req, res) {
  
  
  const unsplash = await new Unsplash(req.body.data);
  const bg_images = await unsplash.init();
  res.status(200).send(bg_images)
  
})

app.post('/getlocation', jsonParser, async function (req, res){

    const getcitydata = await new GetCityData(req.body.latitude, req.body.longitude)  
    const retData = await getcitydata.init();
    
    await res.status(200).send(retData.data.results[0].locations)
    

})

app.get('/getprofile', jsonParser, async function (req, res){

  const profile = new GetLinkedInProfile();
  const profileData = profile.getData();
  
  await res.status(200).send(profileData)
  

})

app.listen(8000)

console.log('listening to 8000')


