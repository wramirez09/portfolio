
const axios = require("axios");

class GetLinkedInProfile {
    constructor(){
        this.endpoint = "http://api.linkedin.com/v2/me",
        this.accessToken = "AQR5zyqJGMqMzsaKFAZuHfIwL1QWANq_X4Bm3JEwDg6d2naoPT3yM6B6PJMgWEKlpxIf9R-3fu0l8tkI-yCC81CpqlnXHMWDL7YYXkEL8ygpLbsvgfhM7eYt9cmOAU09__rf8rlwEfHQ-uWLXAny4D6BBWIyr1MiX_bYnTmyBEaA_Ng9TnxXd6vAM1ltxA"
    }

    getData(){



        return axios({
            method: 'get',
            url: this.endpoint,
            headers: {
                'Authorization': `Bearer ${this.accessToken}`,
                'cache-control': 'no-cache',
                'X-Restli-Protocol-Version': '2.0.0'
              }
          }).then((data)=>{
            return data;
          }).catch((e)=>{
            return console.log("error", e)
          });


          
    }

};

module.exports = GetLinkedInProfile;
// This sample code will make a request to LinkedIn's API to retrieve and print out some
// basic profile information for the user whose access token you provide.
